import css from './css'

const $$ = document.querySelectorAll.bind(document)

export default class ExtNodeList {
  constructor (source) {
    if (typeof source === 'string') {
      this.nodeList = $$(source)
    } else {
      this.nodeList = source
    }
    this.length = this.nodeList.length
    this.forEach = this.nodeList.forEach.bind(this.nodeList)
    this.entries = this.nodeList.entries.bind(this.nodeList)
    this.item = this.nodeList.item.bind(this.nodeList)
    this.keys = this.nodeList.keys.bind(this.nodeList)
    this.values = this.nodeList.values.bind(this.nodeList)
  }

  get () {
    return this.nodeList
  }

  addClass (...args) {
    this.forEach(node => {
      node.classList.add(...args)
    })
  }

  addEventListener (...args) {
    this.forEach(node => {
      node.addEventListener(...args)
    })
  }

  removeClass (...args) {
    this.forEach(node => {
      node.classList.remove(...args)
    })
  }

  replaceClass (...args) {
    this.forEach(node => {
      node.classList.replace(...args)
    })
  }

  toggleClass (...args) {
    this.forEach(node => {
      node.classList.toggle(...args)
    })
  }

  forEachClass (...args) {
    this.forEach(node => {
      node.classList.forEach(...args)
    })
  }

  css (...args) {
    const returnValue = []
    this.forEach(node => {
      const res = css(node, ...args)
      if (res) {
        returnValue.push(res)
      }
    })
    if (returnValue.length) {
      return returnValue
    }
  }
}
